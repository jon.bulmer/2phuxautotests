﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using WebDriverManager;
using WebDriverManager.DriverConfigs.Impl;
using PhuxPOM.Utility;
using Microsoft.Extensions.Configuration;

namespace PhuxPOM.Base
{
    public class DriverSetup
    {
        public static IWebDriver LocalBrowserSetup(IWebDriver driver)
        {
            var configBuilder = new ConfigurationBuilder().
                AddJsonFile("appsettings.json").Build();

            var configSection = configBuilder.GetSection("AppSettings");

            string? browser_name = configSection["browser"] ?? null;
            if (BrowserType.Chrome.ToString().Equals(browser_name))
            {

                ChromeOptions options = new ChromeOptions();
                var extensionString = GetResourcesData.GetData("Environment.json" ,"ChormeExtension");
                options.AddArguments(extensionString);

                new DriverManager().SetUpDriver(new ChromeConfig());
                driver = new ChromeDriver(options);
            }
            else if (BrowserType.Firefox.ToString().Equals(browser_name))
            {
                new DriverManager().SetUpDriver(new FirefoxConfig());
                driver = new FirefoxDriver();
            }
            else if (BrowserType.IE.ToString().Equals(browser_name))
            {
                new DriverManager().SetUpDriver(new InternetExplorerConfig());
                driver = new InternetExplorerDriver();
            }
            else
            {
                Console.WriteLine("Default Browser is initated, please check browser details in app.config file");
                new DriverManager().SetUpDriver(new ChromeConfig());
                driver = new ChromeDriver();
            }

            return driver;
        }
    }
}
