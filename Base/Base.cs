﻿using OpenQA.Selenium;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.Configuration;
using PhuxPOM.WebElements;
using PhuxPOM.Utility;
using PhuxPOM.Pages;

namespace PhuxPOM.Base
{
    public class BaseSetup
    {
        public HomePage? homePage;
        public TourPage? tourPage;
        public DisclaimerPage? disclaimerPage;
        public HeaderPage? headerPage;

        [TestInitialize]
        public void SetupPages() 
        {
            homePage = new(GetDriver());
            tourPage = new(GetDriver());
            disclaimerPage = new(GetDriver());
            headerPage = new(GetDriver());
        }

        public ThreadLocal<IWebDriver> driver = new ThreadLocal<IWebDriver>();

        [ClassInitialize]
        public void Setup()
        {
            string? testclassfilename = "FullQualifiedTestClass";
            string dir = System.Environment.CurrentDirectory;
            string? projdir = Directory.GetParent(dir)?.Parent?.Parent?.FullName;
        }

        [TestInitialize]
        public void Start_Browser()
        {

            SetBrowser();

            string? url = GetResourcesData.GetData();
            ActionsElements.NavigateToUrl(driver.Value, url);
            driver.Value.Manage().Window.Maximize();
        }

        public IWebDriver GetDriver()
        {
            return driver.Value;

        }

        private void SetBrowser()
        {
            var configBuilder = new ConfigurationBuilder().
               AddJsonFile("appsettings.json").Build();

            var configSection = configBuilder.GetSection("AppSettings");

            string? runEnvironment = configSection["run_environment"] ?? null;

            if (runEnvironment != null && runEnvironment.Equals("Local"))
            {

                driver.Value = DriverSetup.LocalBrowserSetup(driver.Value);
            }
            else
            {
                //TestContext.WriteLine("Please check browser name and run enivorment value in app.config file");

            }
        }

        [TestCleanup]
        public void SetTestResults()
        {
            driver?.Value?.Quit();
        }
    }
}
