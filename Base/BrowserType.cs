﻿namespace PhuxPOM.Base
{
    public enum BrowserType
    {
        Chrome,
        Firefox,
        IE
    }
}
