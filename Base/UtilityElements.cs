﻿using PhuxPOM.Utility;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net;

namespace PhuxPOM.Base
{
    public class UtilityElements
    {
        public string btnBackButton = "BackButton";
        public string btnNextButton = "NextButton";
        public string btnCloseButton = "CloseButton";
        public string btnConfirmButton  = "ConfirmButton";

        public string lblHeadingText = "HeadingText";
        public string lblDescriptionText = "DescriptionText";
    }
}
