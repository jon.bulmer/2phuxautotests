﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using System.Runtime.InteropServices;

namespace PhuxPOM.Utility
{
    public class GetResourcesData
    {
        public static string GetData(string fileName = "Environment.json", string data = "WebURL")
        {
            /*
             * Use this method to get URL for the data based on enviornment
             * parameter data : Value you want to fetch from enviornment.json file
             */

            string url = "";
            try
            {
                var configBuilder = new ConfigurationBuilder().
                    AddJsonFile("appsettings.json").Build();

                var configSection = configBuilder.GetSection("AppSettings");

                string? env = configSection["environment"] ?? null;
                string currentdirectory = Directory.GetParent(System.Environment.CurrentDirectory)?.Parent?.Parent?.FullName;
                var resourcesPath = "/Resources/";
                if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows)) { resourcesPath = "\\Resources\\"; }
                string? jsonstring = File.ReadAllText(currentdirectory + resourcesPath + fileName);
                var json = JToken.Parse(jsonstring);
                object? o = json?.SelectToken(env)?.Value<object>(data);
                url = o?.ToString();

            }
            catch (NullReferenceException e)
            {
                throw new Exception("Null Reference error occured in ReadTestData Method");
            }
            catch (Exception e)
            {
                throw new Exception("Error occured in the GetEnvironment Data method, please check the parameters name");
            }

            return url;
        }
    }
}
