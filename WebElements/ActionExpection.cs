﻿namespace PhuxPOM.WebElements
{
    public class ActionExpection : Exception
    {
        public ActionExpection(string message) : base(message)
        {

        }
    }
}