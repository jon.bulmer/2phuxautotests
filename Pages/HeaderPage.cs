﻿using OpenQA.Selenium;
using PhuxPOM.WebElements;
using PhuxPOM.Base;

namespace PhuxPOM.Pages
{
    public class HeaderPage : UtilityElements
    {
        public string btnPoolsButton = "PoolsButton";
        public string btnPrime2PhuxButton = "Prime2PhuxButton";
        public string btnClaimButton = "ClaimButton";
        public string btnPortfolioButton = "PortfolioButton";
        public string btnLockButton = "LockButton";
        public string btnConnectWalletButton = "ConnectWalletButton";
        public string btnG4LinkButton = "G4LinkButton";

        public string chkRpcCheckBox = "RpcCheckBox";

        public string lblRpcText = "RpcText";
        public string lblRpcOnlyText = "RpcOnlyText";

        private readonly IWebDriver driver;
        public HeaderPage(IWebDriver driver)
        {
            this.driver = driver;
        }

        readonly string poolsButton = "//a[text()='Pools']";
        readonly string prime2PhuxButton = "//a[text()='Prime2Phux']";
        readonly string claimButton = "//a[text()='Claim']";
        readonly string portfolioButton = "//a[text()='Portfolio']";
        readonly string lockButton = "//a[text()='Lock']";
        readonly string g4LinkButton = "//a[text()='g4mm4.io']";
        readonly string connectWalletButton = "//button[text()='Connect wallet']";
        //*[@id="__next"]/div[1]/div[1]/div/div[2]/button
        readonly string rpcCheckBox = ".sc-1ecf058b-0";
        readonly string rpcText = "div.sc-ff24db80-0.hkeitR > div > div:nth-child(3) > div > div:nth-child(1) > p";
        readonly string rpcOnlyText = "div.sc-ff24db80-0.hkeitR > div > div:nth-child(3) > div > div:nth-child(2) > p";

        public void ClickButton(string selectedElement)
        {
            if (selectedElement == "PoolsButton") { selectedElement = poolsButton; }
            if (selectedElement == "Prime2PhuxButton") { selectedElement = prime2PhuxButton; }
            if (selectedElement == "ClaimButton") { selectedElement = claimButton; }
            if (selectedElement == "PortfolioButton") { selectedElement = portfolioButton; }
            if (selectedElement == "LockButton") { selectedElement = lockButton; }
            if (selectedElement == "G4LinkButton" ) { selectedElement = g4LinkButton; }
            if (selectedElement == "ConnectWalletButton" ) { selectedElement = connectWalletButton; }
            ActionsElements.Click(driver, By.XPath(selectedElement));
        }

        public string ReturnElementText(string selectedElement)
        {
            if (selectedElement == "RpcText") { selectedElement = rpcText; }
            if (selectedElement == "RpcOnlyText") { selectedElement = rpcOnlyText; }

            var result = ActionsElements.GetText(driver, By.CssSelector(selectedElement));
            return result;
        }

        public void ClickCheckBox(string selectedElement)
        {
            if (selectedElement == "RpcCheckBox") { selectedElement = rpcCheckBox; }
            ActionsElements.Click(driver, By.CssSelector(selectedElement));
        }
    }
}