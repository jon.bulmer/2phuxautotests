﻿using OpenQA.Selenium;
using PhuxPOM.WebElements;
using PhuxPOM.Base;

namespace PhuxPOM.Pages
{
    public class DisclaimerPage : UtilityElements
    {
        public string lblStepCounterText = "StepCounterText";
        public string lblUnderstoodText = "UnderstoodText";
        public string lblAcknowledgeText = "AcknowledgeText";

        public string chkUnderstoodCheckBox = "UnderstoodCheckBox";
        public string chkAcknowledgeCheckBox = "AcknowledgeCheckBox";

        private readonly IWebDriver driver;
        public DisclaimerPage(IWebDriver driver)
        {
            this.driver = driver;
        }

        readonly string confirmButton = ".sc-47ff58fb-15";
        readonly string backButton = ".sc-ffc65308-1 img";

        readonly string headingText = ".sc-47ff58fb-20";
        readonly string descriptionText = ".sc-47ff58fb-21 p p:nth-child(1)";
        readonly string understoodText = ".sc-47ff58fb-21 p p:nth-child(2)";
        readonly string acknowledgeText = ".sc-47ff58fb-21 p p:nth-child(3)";
        readonly string understoodCheckBox = "label:nth-child(3) > span";
        readonly string acknowledgeCheckBox = "label:nth-child(5) > span";

        public void ClickButton(string selectedElement)
        {
            if (selectedElement == "ConfirmButton") { selectedElement = confirmButton; }
            if (selectedElement == "BackButton") { selectedElement = backButton; }
            ActionsElements.Click(driver, By.CssSelector(selectedElement));
        }

        public string ReturnElementText(string selectedElement)
        {
            if (selectedElement == "HeadingText") { selectedElement = headingText; }
            if (selectedElement == "DescriptionText") { selectedElement = descriptionText; }
            if (selectedElement == "UnderstoodText") { selectedElement = understoodText; }
            if (selectedElement == "AcknowledgeText") { selectedElement = acknowledgeText; }

            var result = ActionsElements.GetText(driver, By.CssSelector(selectedElement));
            return result;
        }

        public void ClickCheckBox(string selectedElement)
        {
            if (selectedElement == "UnderstoodCheckBox") { selectedElement = understoodCheckBox; }
            if (selectedElement == "AcknowledgeCheckBox") { selectedElement = acknowledgeCheckBox; }
            ActionsElements.Click(driver, By.CssSelector(selectedElement));
        }

        public bool ElementEnabled(string selectedElement)
        {
            if (selectedElement == "ConfirmButton") { selectedElement = confirmButton; }
            if (selectedElement == "BackButton") { selectedElement = backButton; }
            var result = ActionsElements.IsElementActive(driver, By.CssSelector(selectedElement));
            return result;
        }
    }
}