﻿using OpenQA.Selenium;
using PhuxPOM.WebElements;
using PhuxPOM.Base;

namespace PhuxPOM.Pages
{
    public class HomePage : UtilityElements
    {
        public string btnTakeTourButton = "TakeTourButton";
        public string btnlaunchAppButton = "LaunchAppButton";
        public string btnGetStartedButton = "GetStartedButton";

        public string lblHomeTitleText = "HomeTitleText";
        public string lblDescriptionText = "HomeDescriptionText";

        private readonly IWebDriver driver;
        public HomePage(IWebDriver driver)
        {
            this.driver = driver;
        }

        readonly string takeTourButton = "//button[text()='Take a tour']";
        readonly string launchAppButton = "//a[text()='Launch app']";
        readonly string getStartedButton = "//a[text()='Get started']";

        readonly string homeTitleText = ".sc-e33aee2a-8";
        readonly string homeDescriptionText = ".sc-e33aee2a-9";

        public void ClickButton(string selectedElement)
        {
            if (selectedElement == "TakeTourButton") { selectedElement = takeTourButton; }
            if (selectedElement == "LaunchAppButton") { selectedElement = launchAppButton; }
            if (selectedElement == "GetStartedButton") { selectedElement = getStartedButton; }
            ActionsElements.Click(driver, By.XPath(selectedElement));
        }

        public string ReturnElementText(string selectedElement)
        {
            if (selectedElement == "HomeTitleText") { selectedElement = homeTitleText; }
            if (selectedElement == "HomeDescriptionText") { selectedElement = homeDescriptionText; }

            var result = ActionsElements.GetText(driver, By.CssSelector(selectedElement));
            return result;
        }
    }
}
