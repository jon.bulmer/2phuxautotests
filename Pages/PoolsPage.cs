﻿using OpenQA.Selenium;
using PhuxPOM.WebElements;
using PhuxPOM.Base;

namespace PhuxPOM.Pages
{
    public class PoolsPage : UtilityElements
    {
        public string btnPoolItemButton = "PoolItemButton";
        public string btnPoolItemPagingButton = "PoolItemPagingButton";


        public string lblStepCounterText = "StepCounterText";
        public string lblPoolItemNameText = "PoolItemName";
        public string lblPoolItemCompositionItemText = "PoolItemCompositionItemText";


        public string txtSearchInput = "SearchInput";

        private readonly IWebDriver driver;
        public PoolsPage(IWebDriver driver)
        {
            this.driver = driver;
        }

        readonly string Button = "";
        
        readonly string Text = "";

        public void ClickButton(string selectedElement)
        {
            if (selectedElement == "Button") { selectedElement = Button; }
            ActionsElements.Click(driver, By.CssSelector(selectedElement));
        }

        public string ReturnElementText(string selectedElement)
        {
            if (selectedElement == "Text") { selectedElement = Text; }

            var result = ActionsElements.GetText(driver, By.CssSelector(selectedElement));
            return result;
        }
    }
}
