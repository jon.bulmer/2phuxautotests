﻿using OpenQA.Selenium;
using PhuxPOM.WebElements;
using PhuxPOM.Base;

namespace PhuxPOM.Pages
{
    public class TourPage : UtilityElements
    {
        public string lblStepCounterText = "StepCounterText";

        private readonly IWebDriver driver;
        public TourPage(IWebDriver driver)
        {
            this.driver = driver;
        }

        readonly string nextButton = ".sc-88e0156d-4";
        readonly string backButton = ".sc-88e0156d-3";
        readonly string closeButton = ".sc-88e0156d-1";

        readonly string headingText = ".sc-88e0156d-6";
        readonly string descriptionText = ".sc-88e0156d-7";
        readonly string stepCounterText = ".sc-88e0156d-5";

        public void ClickButton(string selectedElement)
        {
            if (selectedElement == "NextButton") { selectedElement = nextButton; }
            if (selectedElement == "BackButton") { selectedElement = backButton; }
            if (selectedElement == "CloseButton") { selectedElement = closeButton; }
            ActionsElements.Click(driver, By.CssSelector(selectedElement));
        }

        public string ReturnElementText(string selectedElement)
        {
            if (selectedElement == "HeadingText") { selectedElement = headingText; }
            if (selectedElement == "DescriptionText") { selectedElement = descriptionText; }
            if (selectedElement == "StepCounterText") { selectedElement = stepCounterText; }

            var result = ActionsElements.GetText(driver, By.CssSelector(selectedElement));
            return result;
        }
    }
}
