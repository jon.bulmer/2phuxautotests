﻿using PhuxPOM.Pages;
using PhuxPOM.Base;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;

namespace PhuxPOM.TestSuites
{
    [TestClass]
    public class DescliamerPageTests : BaseSetup
    {
        public ThreadLocal<IWebDriver> driver = new ThreadLocal<IWebDriver>();

        [TestMethod]
        public void Acknowlege_Checkbox_Checked_Works()
        {
            homePage.ClickButton(homePage.btnlaunchAppButton);
            disclaimerPage.ClickCheckBox(disclaimerPage.chkAcknowledgeCheckBox);
            Assert.AreEqual(false, disclaimerPage.ElementEnabled(disclaimerPage.btnConfirmButton));
        }
    }
}
