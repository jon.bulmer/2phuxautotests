﻿using PhuxPOM.Pages;
using PhuxPOM.Base;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;

namespace PhuxPOM.TestSuites
{
    [TestClass]
    public class HeaderPagePageTests : BaseSetup
    {
        public ThreadLocal<IWebDriver> driver = new ThreadLocal<IWebDriver>();

        [TestMethod]
        public void Header_Buttons_Works_As_Expected()
        {
            homePage.ClickButton(homePage.btnlaunchAppButton);
            disclaimerPage.ClickButton(disclaimerPage.btnBackButton);
            headerPage.ClickButton(headerPage.btnPoolsButton);
            headerPage.ClickButton(headerPage.btnPortfolioButton);
            headerPage.ClickButton(headerPage.btnClaimButton);
            headerPage.ClickButton(headerPage.btnPrime2PhuxButton);
            //headerPage.ClickButton(headerPage.btnG4LinkButton);
            headerPage.ClickButton(headerPage.btnLockButton);
            //headerPage.ClickButton(headerPage.btnConnectWalletButton);
        }

        [TestMethod]
        public void Header_CheckBox_Works_As_Expected()
        {
            homePage.ClickButton(homePage.btnlaunchAppButton);
            disclaimerPage.ClickButton(disclaimerPage.btnBackButton);
            headerPage.ClickCheckBox(headerPage.chkRpcCheckBox);
        }

        [TestMethod]
        public void Header_Labels_Are_Correct()
        {
            homePage.ClickButton(homePage.btnlaunchAppButton);
            disclaimerPage.ClickButton(disclaimerPage.btnBackButton);
            Assert.AreEqual("RPC Only", headerPage.ReturnElementText(headerPage.lblRpcOnlyText));
            Assert.AreEqual("RPC Powered by <a href=\"g4mm4.io\">g4mm4.io</a>", headerPage.ReturnElementText(headerPage.lblRpcText));
        }
    }
}