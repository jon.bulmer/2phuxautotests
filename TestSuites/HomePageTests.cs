﻿using PhuxPOM.Pages;
using PhuxPOM.Base;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;

namespace PhuxPOM.TestSuites
{
    [TestClass]
    public class HomePageTests : BaseSetup
    {
        public ThreadLocal<IWebDriver> driver = new ThreadLocal<IWebDriver>();

        [TestMethod]
        public void Take_Tour_Button_Navigation_Works()
        {
            homePage.ClickButton(homePage.btnTakeTourButton);
            Assert.AreEqual("Your Wallet", tourPage.ReturnElementText(tourPage.lblHeadingText)); 
        }

        [TestMethod]
        public void Launch_App_Button_Navigation_Works()
        {
            homePage.ClickButton(homePage.btnlaunchAppButton);
            Assert.AreEqual("Disclaimer",
                disclaimerPage.ReturnElementText(disclaimerPage.lblHeadingText));
        }

        [TestMethod]
        public void Get_Started_Button_Navigation_Works()
        {
            homePage.ClickButton(homePage.btnGetStartedButton);
            Assert.AreEqual("Disclaimer",
                disclaimerPage.ReturnElementText(disclaimerPage.lblHeadingText));
        }

        [TestMethod]
        public void Header_Text_Is_Correct()
        {
            Assert.AreEqual("We give more Phux",
                homePage.ReturnElementText(homePage.lblHomeTitleText));
        }

        [TestMethod]
        public void Description_Text_Is_Correct()
        {
            Assert.AreEqual("Unlocking",
                homePage.ReturnElementText(homePage.lblDescriptionText).Substring(0, 9));
        }
    }
}
